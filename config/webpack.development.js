var webpack = require("webpack"),
  path = require("path"),
  webpackMerge = require("webpack-merge"),
  commonConfig = require("./webpack.common");

module.exports = function(env) {
  return webpackMerge(commonConfig(env), {
    devtool: "eval",
    devServer: {
      // stats: "minimal",
      hot: true,
      inline: true,
      historyApiFallback: true,
      contentBase: path.resolve(__dirname, "../src"),
      port: 3001
      // publicPath: "http://localhost:3001/",
      // outputPath: path.resolve(__dirname, "../dist"),
      // compress: true,
      // hotOnly: true,
      // overlay: {
      //     warnings: true,
      //     errors: true
      // }
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: [
            "@angularclass/hmr-loader",
            "awesome-typescript-loader",
            "angular2-template-loader",
            "angular2-router-loader"
          ],
          exclude: /node_modules/,
          include: path.resolve(__dirname, "../src")
        },
        {
          test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
          use: ["file-loader", "url-loader?limit=25000"],
          include: [path.resolve(__dirname, "../src/assets")]
        },
        {
          // sadece global style ler için (./src/styles)
          test: /\.css$/,
          use: ["style-loader", "css-loader?importLoaders=1", "postcss-loader"],
          include: [path.resolve(__dirname, "../src/styles")]
        }
      ]
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin()
    ]
  });
};
