var webpack = require("webpack"),
  path = require("path"),
  HtmlWebpackPlugin = require("html-webpack-plugin"),
  ChunkManifestPlugin = require("chunk-manifest-webpack-plugin"),
  WebpackChunkHash = require("webpack-chunk-hash"),
  CheckerPlugin = require("awesome-typescript-loader").CheckerPlugin;

module.exports = function(env) {
  return {
    context: path.resolve(__dirname, "../src"),
    entry: {
      polyfills: "./polyfills.ts",
      vendor: "./vendor.ts",
      main: "./main.ts"
    },
    output: {
      path: path.resolve(__dirname, "../dist"),
      publicPath: "/",
      filename: "[name]" +
        (env.production ? ".[chunkhash:8]" : "") +
        ".bundle.js",
      sourceMapFilename: "[name]" +
        (env.production ? ".[chunkhash:8]" : "") +
        ".bundle.js.map",
      chunkFilename: "[name]" +
        (env.production ? ".[chunkhash:8]" : "") +
        ".chunk.js"
    },
    resolve: {
      extensions: [".js", ".ts"],
      modules: [
        path.resolve(__dirname, "../src"),
        path.resolve(__dirname, "../node_modules")
      ]
    },
    resolveLoader: {
      modules: [path.resolve(__dirname, "../node_modules")]
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: ["awesome-typescript-loader"],
          exclude: [path.resolve(__dirname, "../src"), /node_modules/]
        },
        {
          // sadece angular component leri için (./src/app)
          test: /\.css$/,
          use: [
            "to-string-loader",
            "css-loader?importLoaders=1",
            "postcss-loader"
          ],
          include: [path.resolve(__dirname, "../src/app")]
        },
        {
          test: /\.html$/,
          use: ["html-loader"],
          include: [path.resolve(__dirname, "../src")]
        },
        {
          test: /\.pug$/,
          use: ["pug-loader"],
          include: [path.resolve(__dirname, "../src")]
        },
        {
          test: /\.json$/,
          use: ["json-loader"],
          include: [path.resolve(__dirname, "../src")]
        }
      ]
    },
    plugins: [
      new webpack.NoEmitOnErrorsPlugin(),
      new webpack.ProgressPlugin({
        colors: true
      }),
      new HtmlWebpackPlugin({
        template: "./index.html",
        inject: "body"
      }),
      new webpack.EnvironmentPlugin({
        NODE_ENV: env.development
          ? "development"
          : env.production ? "production" : env.test ? "test" : "development"
      }),
      new webpack.optimize.CommonsChunkPlugin({
        name: ["main", "vendor", "polyfills", "manifest"], // manifest = webpack logic
        minChunks: Infinity
      }),
      new webpack.HashedModuleIdsPlugin(),
      new WebpackChunkHash(),
      new ChunkManifestPlugin(),
      new webpack.ContextReplacementPlugin(
        /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
        path.resolve(__dirname, "../src")
      ),
      new CheckerPlugin()
    ],
    node: {
      fs: "empty",
      global: true,
      crypto: "empty",
      tls: "empty",
      net: "empty",
      process: true,
      module: false,
      clearImmediate: false,
      setImmediate: false
    }
  };
};
