var webpack = require("webpack"),
  path = require("path"),
  webpackMerge = require("webpack-merge"),
  commonConfig = require("./webpack.common"),
  ngToolsWebpack = require("@ngtools/webpack"),
  CompressionPlugin = require("compression-webpack-plugin"),
  ExtractTextPlugin = require("extract-text-webpack-plugin"),
  OptimizeJsPlugin = require("optimize-js-plugin"),
  CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = function(env) {
  return webpackMerge(commonConfig(env), {
    devtool: "source-map",
    performance: {
      // maxAssetSize: 100000,
      // maxEntrypointSize: 300000,
      hints: true
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: ["@ngtools/webpack"],
          exclude: /node_modules/,
          include: path.resolve(__dirname, "../src")
        },
        {
          // sadece global style ler için (./src/styles)
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: ["css-loader?importLoaders=1", "postcss-loader"]
          }),
          include: [path.resolve(__dirname, "../src/styles")]
        },
        {
          test: /.*\.(png|gif|jpe?g|svg)$/,
          use: [
            "file-loader?hash=sha512&digest=hex&name=[hash].[ext]",
            "url-loader?limit=25000",
            "image-webpack-loader?{optimizationLevel: 7, interlaced: false, pngquant:{quality: '65-90', speed: 4}, mozjpeg: {quality: 65}}"
          ],
          include: [path.resolve(__dirname, "../src/assets")]
        },
        {
          test: /\.(woff|woff2|ttf|eot|ico)$/,
          use: ["file-loader", "url-loader?limit=25000"],
          include: [path.resolve(__dirname, "../src/assets")]
        }
      ]
    },
    plugins: [
      // new webpack.LoaderOptionsPlugin({
      //     minimize: true,
      //     debug: false
      // }),
      new ngToolsWebpack.AotPlugin({
        tsConfigPath: path.resolve(__dirname, "../tsconfig.json"),
        entryModule: path.resolve(__dirname, "../src/app/app.module#AppModule"),
        mainPath: path.resolve(__dirname, "../src/main.ts")
      }),
      new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        mangle: {
          screw_ie8: true,
          keep_fnames: true
        },
        compress: {
          warnings: false,
          screw_ie8: true
        },
        sourceMap: true,
        comments: false
      }),
      new OptimizeJsPlugin({
        sourceMap: true
      }),
      new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.(js|html)$/,
        threshold: 10240,
        minRatio: 0.8
      }),
      new ExtractTextPlugin("[name].[contenthash:8].css"),
      new CopyWebpackPlugin([
        {
          from: path.resolve(__dirname, "../src/assets"),
          to: path.resolve(__dirname, "../dist/assets")
        }
      ])
    ]
  });
};
